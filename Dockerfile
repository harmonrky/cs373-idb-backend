FROM python:3.6
ENV PYTHONUNBUFFERED 1
RUN apt-get update && apt-get upgrade -y && apt-get autoremove && apt-get autoclean
RUN apt-get install default-libmysqlclient-dev

# Project Files and Settings
RUN mkdir /idb_django
WORKDIR /idb_django
ADD . /idb_django
RUN pip install -r requirements.txt
# Server
EXPOSE 5000
COPY ./entrypoint.sh /idb_django
ENTRYPOINT ["./entrypoint.sh"]
