#!/bin/bash

export DJANGO_SETTINGS_MODULE=idb_backend.settings

exec gunicorn idb_backend.wsgi:application \
    --bind 0.0.0.0:5000 \
    --workers 3 \
