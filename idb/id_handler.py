import requests
from django.core.paginator import *
from django.db.models import FieldDoesNotExist

from .models import SchoolDistricts, Communities, Charities

MAX_LIST_ENTRIES = 8
SEARCH_URL = "https://idb-search-dot-cs373-idb-218121.appspot.com/search_doc"

SEARCH_ENTITY = {
    SchoolDistricts: "school_districts",
    Communities: "communities"
}


def create_school_response(entity):
    return {
        "Id": entity.id,
        "Name": entity.name,
        "Image": entity.image,
        "Info": {
            "State": entity.state,
            "Community": entity.community,
            "Poverty": str(entity.poverty) + "%",
            "Population": entity.population,
            "Summary": entity.summary,
        }
    }


def create_communities_response(entity):
    return {
        "Id": entity.id,
        "Name": entity.community,
        "Image": entity.image,
        "Location": entity.coordinates,
        "Info": {
            "State": entity.state,
            "Counties": entity.counties,
            "Population": entity.population,
            "Summary": entity.summary,
        }
    }


def create_charities_response(entity):
    return {
        "Id" : entity.id,
        "Name" : entity.name,
        "Image" : entity.image,
        "Info" : {
            "Tagline" : entity.tagline,
            "Category" : entity.category,
            "Scope" : entity.scope,
            "State" : entity.state,
            "Website" : entity.website,
            "Mission" : entity.mission,
            "Rating" : entity.rating,
        }
    }

FORMAT_RETURN_ENTITY = {
    SchoolDistricts: create_school_response,
    Communities: create_communities_response,
    Charities: create_charities_response
}


def id_handler(entity_type, request):
    request_id = request.GET['id']
    
    if request_id is None:
        raise ValueError
    entity = entity_type.objects.get(pk=int(request_id))
    return FORMAT_RETURN_ENTITY[entity_type](entity)
