# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Charities(models.Model):
    name = models.CharField(max_length=150)
    tagline = models.CharField(max_length=250, blank=True, null=True)
    state = models.CharField(max_length=30)
    category = models.CharField(max_length=60)
    mission = models.TextField()
    scope = models.CharField(max_length=40)
    website = models.TextField(blank=True, null=True)
    image = models.TextField()
    rating = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'charities'


class Communities(models.Model):
    community = models.CharField(max_length=50)
    image = models.CharField(max_length=200)
    summary = models.CharField(max_length=5000)
    state = models.CharField(max_length=20)
    counties = models.CharField(max_length=80)
    population = models.IntegerField()
    coordinates = models.CharField(max_length=80)

    class Meta:
        managed = False
        db_table = 'communities'


class SchoolDistricts(models.Model):
    name = models.CharField(max_length=100)
    poverty = models.DecimalField(max_digits=4, decimal_places=1, blank=True, null=True)
    time = models.IntegerField()
    state = models.CharField(max_length=20)
    district_code = models.IntegerField()
    community = models.CharField(max_length=40)
    image = models.CharField(max_length=200)
    population = models.IntegerField()
    summary = models.TextField()

    class Meta:
        managed = False
        db_table = 'school_districts'
