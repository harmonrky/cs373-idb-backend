import requests
from django.core.paginator import *
from django.db.models import FieldDoesNotExist

from .models import SchoolDistricts, Communities, Charities

MAX_LIST_ENTRIES = 8
SEARCH_URL = "https://idb-search-dot-cs373-idb-218121.appspot.com/search_doc"

SEARCH_ENTITY = {
    SchoolDistricts: "school_districts",
    Communities: "communities",
    Charities: "charities"
}

POPULATION_FILTERS = {
    SchoolDistricts: [(0, 2000), (2001,5000), (5001,)],
    Communities: [(0, 2000), (2001, 10000), (10001,)]
}


def create_school_response(entity):
    return {
        "Id": entity.id,
        "Name": entity.name,
        "Image": entity.image,
        "Model": "school_districts",
        "Info": {
            "Name": entity.name,
            "State": entity.state,
            "Community": entity.community,
            "Poverty": str(entity.poverty) + "%",
            "Population": entity.population
        }
    }


def create_communities_response(entity):
    return {
        "Id" : entity.id,
        "Name" : entity.community,
        "Image" : entity.image,
        "Model" : "communities",
        "Info" : {
            "Name" : entity.community,
            "State" : entity.state,
            "Counties" : entity.counties,
            "Population" : entity.population,
        }
    }


def create_charities_response(entity):
    return {
        "Id": entity.id,
        "Name": entity.name,
        "Image": entity.image,
        "Model": "charities",
        "Info": {
            "Name": entity.name,
            "Tagline": entity.tagline,
            "Category": entity.category,
            "Scope": entity.scope,
            "State": entity.state,
            "Rating": entity.rating,
        }
    }


FORMAT_RETURN_ENTITY = {
    SchoolDistricts: create_school_response,
    Communities: create_communities_response,
    Charities: create_charities_response
}


def get_max_list_per_page(request):
    if 'list' in request.GET:
        return int(request.GET['list'])
    return 9


def query_index(entity_type, search_term):
    search_params = {'query': search_term, 'entity': SEARCH_ENTITY[entity_type]}
    search_response = requests.get(url=SEARCH_URL, params=search_params)
    return search_response.json()["results"]


def search_handler(entity_type, request, entity_list):
    if 'search' in request.GET:
        search_term = request.GET['search']
        if search_term == '':
            raise ValueError
        search_results_list = query_index(entity_type, search_term)
        return entity_list.filter(pk__in=search_results_list)
    return entity_list


def filter_handler(entity_type, request, entity_list):
    if 'state' in request.GET:
        entity_list = entity_list.filter(state=request.GET['state'].title())
    if 'poverty' in request.GET:
        if request.GET['poverty'] == 'moderate':
            entity_list = entity_list.filter(poverty__gte=0, poverty__lte=45.0)
        elif request.GET['poverty'] == 'severe':
            entity_list = entity_list.filter(poverty__gte=45.1, poverty__lte=50.0)
        elif request.GET['poverty'] == 'extreme':
            entity_list = entity_list.filter(poverty__gte=50.1)

    if 'population' in request.GET:
        population_filters = POPULATION_FILTERS[entity_type]
        if request.GET['population'] == 'small':
            entity_list = entity_list.filter(population__gte=population_filters[0][0], population__lte=population_filters[0][1])
        elif request.GET['population'] == 'medium':
            entity_list = entity_list.filter(population__gte=population_filters[1][0], population__lte=population_filters[1][1])
        elif request.GET['population'] == 'large':
            entity_list = entity_list.filter(population__gte=population_filters[2][0])

    if 'scope' in request.GET:
        entity_list = entity_list.filter(scope=request.GET['scope'].title())

    if "category" in request.GET:
        entity_list = entity_list.filter(category=request.GET['category'].title())

    if 'rating' in request.GET:
        if request.GET['rating'] == 'low':
            entity_list = entity_list.filter(rating__gte=0, rating__lte=1)
        elif request.GET['rating'] == 'average':
            entity_list = entity_list.filter(rating=2)
        elif request.GET['rating'] == 'high':
            entity_list = entity_list.filter(rating__gte=3)
    return entity_list


def sort_handler(entity_type, request, entity_list):
    try:
        if 'sort' in request.GET and entity_type._meta.get_field(request.GET['sort']):
            if "desc" in request.GET and request.GET['desc'] == "true":
                entity_list = entity_list.order_by('-' + request.GET['sort'])
            else:
                entity_list = entity_list.order_by(request.GET['sort'])
    except FieldDoesNotExist:
        pass
    return entity_list


def page_handler(entity_type, request):
    page_num = request.GET['page']
    if page_num is None:
        raise ValueError
    MAX_LIST_PER_PAGE = get_max_list_per_page(request)

    entity_list = entity_type.objects.all()
    entity_list = search_handler(entity_type, request, entity_list)
    entity_list = filter_handler(entity_type, request, entity_list)
    entity_list = sort_handler(entity_type, request, entity_list)

    paginator = Paginator(entity_list, MAX_LIST_PER_PAGE)
    entity_page = paginator.page(page_num)

    return_list = [FORMAT_RETURN_ENTITY[entity_type](entity) for entity in entity_page]
    return {"num_pages": paginator.num_pages, "grid": return_list}
