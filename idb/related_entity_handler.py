import requests
from django.core.paginator import *
from django.db.models import FieldDoesNotExist

from .models import SchoolDistricts, Communities, Charities

MAX_LIST_ENTRIES = 8
SEARCH_URL = "https://idb-search-dot-cs373-idb-218121.appspot.com/search_doc"


def get_related_entity_list(entity_type, related_entity_type, related_entity_id):
    if related_entity_id is None:
        raise ValueError
    related_entity = related_entity_type.objects.get(pk=int(related_entity_id))
    if isinstance(related_entity, Communities):
        entity_list = entity_type.objects.filter(community=related_entity.community)[:MAX_LIST_ENTRIES]
    elif isinstance(related_entity, Charities):
        if related_entity.scope == 'Regional':
            entity_list = entity_type.objects.filter(state=related_entity.state)[:MAX_LIST_ENTRIES]
        else:
            entity_list = entity_type.objects.all()[:MAX_LIST_ENTRIES]

    return_list = []
    for entity in entity_list:
        return_dict = {
            "Id": entity.id,
            "Name": entity.name,
        }
        return_list.append(return_dict)
    return return_list
