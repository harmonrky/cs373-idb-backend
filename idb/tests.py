# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import RequestFactory, TestCase
from .models import SchoolDistricts, Communities, Charities
from .views import *
request_inputs = {
	'school_districts_id' : '/school_districts?id=500',
	'school_districts_page1' : '/school_districts?page=1',
	'school_districts_page2' : '/school_districts?page=1&list=2',
	'school_districts_sort1' : '/school_districts?page=1&sort=name&list=1',
	'school_districts_sort2' : '/school_districts?page=1&sort=population&list=2',
	'school_districts_filter1' : '/school_districts?page=1&state=texas&list=1',
	'school_districts_search' : '/school_districts?page=1&search=texas&list=1',
	'school_districts_charity_id' : '/school_districts?charity_id=358',
	'school_districts_community_id' : '/school_districts?community_id=353',
	'communities_id' : '/communities?id=353',
	'communities_page1' : '/communities?page=1',
	'communities_page2' : '/communities?page=1&list=2',
	'communities_sort1' : '/communities?page=1&sort=community&list=1',
	'communities_sort2' : '/communities?page=1&sort=population&list=2',
	'communities_filter1' : '/communities?page=1&state=texas&list=1',
	'communities_search' : '/communities?page=1&search=texas&list=1',
	'communities_charity_id' : '/communities?charity_id=358',
	'communities_school_district_id' : '/communities?school_district_id=500',
	'charities_id' : '/charities?id=353',
	'charities_page1' : '/charities?page=1',
	'charities_page2' : '/charities?page=1&list=2',
	'charities_sort1' : '/charities?page=1&sort=name&list=1',
	'charities_sort2' : 'charities?page=1&sort=rating&list=2',
	'charities_filter1' : '/charities?page=1&state=texas&list=1',
	'charities_search' : '/charities?page=1&search=texas&list=1',
	'charities_community_id' : '/charities?community_id=324',
	'charities_school_district_id' : '/charities?school_district_id=500',
	'all_entities_search1' : '/all?page=1&search=texas',

}
response_outputs = {
	'school_districts_id' : '{"Id": 500, "Name": "Allendale County School District", "Image": "https://d6vze32yv269z.cloudfront.net/organizations/b63366fb-91b5-4077-83bc-516353eb33cc/malrwr-allendale_logo.png", "Info": {"State": "South Carolina", "Community": "Allendale, SC", "Poverty": "49.1%", "Population": 2662, "Summary": "Allendale County Schools is a public school district located in Fairfax, SC. It has 1,265 students in grades PK, K-12 with a student-teacher ratio of 15 to 1. According to state test scores, 22% of students are at least proficient in math and 20% in reading."}}',
	'school_districts_page1' : '{"num_pages": 16, "grid": [{"Id": 451, "Name": "Clarksdale Municipal School District", "Image": "https://scontent.fftw1-1.fna.fbcdn.net/v/t1.0-9/1378464_178196972373520_606778070_n.png?_nc_cat=109&_nc_ht=scontent.fftw1-1.fna&oh=57e1e441edd31ec12df2730f8cd22b82&oe=5C3CC584", "Model": "school_districts", "Info": {"Name": "Clarksdale Municipal School District", "State": "Mississippi", "Community": "Clarksdale, MS", "Poverty": "71.0%", "Population": 4594}}, {"Id": 452, "Name": "Coahoma County School District", "Image": "https://bloximages.chicago2.vip.townnews.com/pressregister.com/content/tncms/assets/v3/editorial/2/54/254cf7d6-39c3-11e4-82b9-133527bc7507/5411b7e16ca75.image.jpg?resize=760%2C580", "Model": "school_districts", "Info": {"Name": "Coahoma County School District", "State": "Mississippi", "Community": "Clarksdale, MS", "Poverty": "67.7%", "Population": 1965}}, {"Id": 453, "Name": "Brooklyn Community Unit School District 188", "Image": "http://www.lovejoy.stclair.k12.il.us/lovejoy_school.jpg", "Model": "school_districts", "Info": {"Name": "Brooklyn Community Unit School District 188", "State": "Illinois", "Community": "Brooklyn, IL", "Poverty": "62.8%", "Population": 218}}, {"Id": 454, "Name": "Quemado Independent Schools", "Image": "http://images.pcmac.org//images/Users/webmaster@quemadosd.com//bald-eagle-clipart-16.gif", "Model": "school_districts", "Info": {"Name": "Quemado Independent Schools", "State": "New Mexico", "Community": "Quemado, NM", "Poverty": "62.7%", "Population": 252}}, {"Id": 455, "Name": "Lowndes County School District", "Image": "https://www.nowforce.com/wp-content/uploads/2014/12/lowndes-county1.png", "Model": "school_districts", "Info": {"Name": "Lowndes County School District", "State": "Alabama", "Community": "Hayneville, AL", "Poverty": "60.4%", "Population": 2851}}, {"Id": 456, "Name": "Buena Vista Independent School District", "Image": "https://www.bvisd.net/assets/apptegy_cms/themes/buenavistatx/logo-c72a6df53f9c4f4e2f78dc04ecead867.png", "Model": "school_districts", "Info": {"Name": "Buena Vista Independent School District", "State": "Texas", "Community": "Imperial, TX", "Poverty": "60.3%", "Population": 113}}, {"Id": 457, "Name": "East Carroll Parish School District", "Image": "https://www.e-carrollschools.org/cms/lib/LA02201178/Centricity/Template/GlobalAssets/images///logos/logo.png", "Model": "school_districts", "Info": {"Name": "East Carroll Parish School District", "State": "Louisiana", "Community": "Lake Providence, LA", "Poverty": "58.9%", "Population": 2153}}, {"Id": 458, "Name": "Selma City School District", "Image": "http://www.selmacityschools.org/sysimages/logoTopNew2.jpg", "Model": "school_districts", "Info": {"Name": "Selma City School District", "State": "Alabama", "Community": "Selma, AL", "Poverty": "57.6%", "Population": 6182}}, {"Id": 459, "Name": "Fulton Independent School District", "Image": "https://pbs.twimg.com/profile_images/903370336414171138/i8_mo-Ww_400x400.jpg", "Model": "school_districts", "Info": {"Name": "Fulton Independent School District", "State": "Kentucky", "Community": "Hickman, KY", "Poverty": "57.1%", "Population": 629}}]}',
	'school_districts_page2' : '{"num_pages": 70, "grid": [{"Id": 451, "Name": "Clarksdale Municipal School District", "Image": "https://scontent.fftw1-1.fna.fbcdn.net/v/t1.0-9/1378464_178196972373520_606778070_n.png?_nc_cat=109&_nc_ht=scontent.fftw1-1.fna&oh=57e1e441edd31ec12df2730f8cd22b82&oe=5C3CC584", "Model": "school_districts", "Info": {"Name": "Clarksdale Municipal School District", "State": "Mississippi", "Community": "Clarksdale, MS", "Poverty": "71.0%", "Population": 4594}}, {"Id": 452, "Name": "Coahoma County School District", "Image": "https://bloximages.chicago2.vip.townnews.com/pressregister.com/content/tncms/assets/v3/editorial/2/54/254cf7d6-39c3-11e4-82b9-133527bc7507/5411b7e16ca75.image.jpg?resize=760%2C580", "Model": "school_districts", "Info": {"Name": "Coahoma County School District", "State": "Mississippi", "Community": "Clarksdale, MS", "Poverty": "67.7%", "Population": 1965}}]}',
	'school_districts_sort1' : '{"num_pages": 140, "grid": [{"Id": 568, "Name": "Aleutian Region School District", "Image": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/NikolskiSchool.jpg/220px-NikolskiSchool.jpg", "Model": "school_districts", "Info": {"Name": "Aleutian Region School District", "State": "Alaska", "Community": "Anchorage, AK", "Poverty": "44.8%", "Population": 65}}]}',
	'school_districts_sort2' : '{"num_pages": 70, "grid": [{"Id": 568, "Name": "Aleutian Region School District", "Image": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/NikolskiSchool.jpg/220px-NikolskiSchool.jpg", "Model": "school_districts", "Info": {"Name": "Aleutian Region School District", "State": "Alaska", "Community": "Anchorage, AK", "Poverty": "44.8%", "Population": 65}}, {"Id": 456, "Name": "Buena Vista Independent School District", "Image": "https://www.bvisd.net/assets/apptegy_cms/themes/buenavistatx/logo-c72a6df53f9c4f4e2f78dc04ecead867.png", "Model": "school_districts", "Info": {"Name": "Buena Vista Independent School District", "State": "Texas", "Community": "Imperial, TX", "Poverty": "60.3%", "Population": 113}}]}',
	'school_districts_filter1' : '{"num_pages": 21, "grid": [{"Id": 456, "Name": "Buena Vista Independent School District", "Image": "https://www.bvisd.net/assets/apptegy_cms/themes/buenavistatx/logo-c72a6df53f9c4f4e2f78dc04ecead867.png", "Model": "school_districts", "Info": {"Name": "Buena Vista Independent School District", "State": "Texas", "Community": "Imperial, TX", "Poverty": "60.3%", "Population": 113}}]}',
	'school_districts_search' : '{"num_pages": 20, "grid": [{"Id": 464, "Name": "Donna Independent School District", "Image": "http://www.donnaisd.net/Portals/Donna/District/images/DISD%20logo%20black%20feather.jpg?ver=2018-08-21-153142-663", "Model": "school_districts", "Info": {"Name": "Donna Independent School District", "State": "Texas", "Community": "Donna, TX", "Poverty": "55.8%", "Population": 29573}}]}',
	'school_districts_charity_id' : '{"School Districts": [{"Id": 456, "Name": "Buena Vista Independent School District"}, {"Id": 464, "Name": "Donna Independent School District"}, {"Id": 470, "Name": "Roma Independent School District"}, {"Id": 472, "Name": "Laredo Independent School District"}, {"Id": 473, "Name": "Edcouch-Elsa Independent School District"}, {"Id": 477, "Name": "Fabens Independent School District"}, {"Id": 480, "Name": "Austwell-Tivoli Independent School District"}, {"Id": 482, "Name": "La Joya Independent School District"}]}',
	'school_districts_community_id' : '{"School Districts": [{"Id": 567, "Name": "Buckholts Independent School District"}]}',
	'communities_id' : '{"Id": 353, "Name": "Buckholts, TX", "Image": "https://upload.wikimedia.org/wikipedia/commons/5/5e/Buckholts_Texas_Town_Hall.jpg", "Location": "30.873889,-97.125000", "Info": {"State": "Texas", "Counties": "Milam County", "Population": 387, "Summary": "Buckholts is a town in Milam County, Texas, United States. The population was 387 at the 2000 census. The town was founded in 1887 and had a movie theater and a hospital in the early 1920s, with a population of approximately one thousand people."}}',
	'communities_page1' : '{"num_pages": 15, "grid": [{"Id": 243, "Name": "Clarksdale, MS", "Image": "https://upload.wikimedia.org/wikipedia/commons/8/8c/Coahoma_County_Mississippi_Incorporated_and_Unincorporated_areas_Clarksdale_Highlighted.svg", "Model": "communities", "Info": {"Name": "Clarksdale, MS", "State": "Mississippi", "Counties": "Coahoma County", "Population": 17962}}, {"Id": 244, "Name": "Brooklyn, IL", "Image": "https://upload.wikimedia.org/wikipedia/commons/b/bf/Clair_County_Illinois_Incorporated_and_Unincorporated_areas_Brooklyn_Highlighted.svg", "Model": "communities", "Info": {"Name": "Brooklyn, IL", "State": "Illinois", "Counties": "St. Clair County", "Population": 749}}, {"Id": 245, "Name": "Quemado, NM", "Image": "https://upload.wikimedia.org/wikipedia/commons/1/10/USA_New_Mexico_location_map.svg", "Model": "communities", "Info": {"Name": "Quemado, NM", "State": "New Mexico", "Counties": "Catron County", "Population": 228}}, {"Id": 246, "Name": "Hayneville, AL", "Image": "https://upload.wikimedia.org/wikipedia/commons/4/43/Hayneville%2C_Alabama.JPG", "Model": "communities", "Info": {"Name": "Hayneville, AL", "State": "Alabama", "Counties": "Lowndes County", "Population": 932}}, {"Id": 247, "Name": "Imperial, TX", "Image": "https://upload.wikimedia.org/wikipedia/commons/1/1a/Sand_dunes_at_Imperial%2C_TX_SCN1050.JPG", "Model": "communities", "Info": {"Name": "Imperial, TX", "State": "Texas", "Counties": "Pecos County", "Population": 278}}, {"Id": 248, "Name": "Lake Providence, LA", "Image": "https://upload.wikimedia.org/wikipedia/commons/b/b0/Louisiana_State_Cotton_Museum_in_Lake_Providence%2C_LA_IMG_7379.JPG", "Model": "communities", "Info": {"Name": "Lake Providence, LA", "State": "Louisiana", "Counties": "East Carroll Parish", "Population": 3991}}, {"Id": 249, "Name": "Selma, AL", "Image": "https://upload.wikimedia.org/wikipedia/commons/2/2f/St._James_Hotel%2C_Selma%2C_Alabama_Highsmith.jpg", "Model": "communities", "Info": {"Name": "Selma, AL", "State": "Alabama", "Counties": "Dallas County", "Population": 20756}}, {"Id": 250, "Name": "Hickman, KY", "Image": "https://upload.wikimedia.org/wikipedia/commons/3/3c/Fulton_County_Courthouse.jpg", "Model": "communities", "Info": {"Name": "Hickman, KY", "State": "Kentucky", "Counties": "Fulton County", "Population": 2395}}, {"Id": 251, "Name": "Durant, MS", "Image": "https://upload.wikimedia.org/wikipedia/commons/9/9c/Depot_-_Durant%2C_Mississippi.jpg", "Model": "communities", "Info": {"Name": "Durant, MS", "State": "Mississippi", "Counties": "Holmes County", "Population": 2673}}]}',
	'communities_page2' : '{"num_pages": 68, "grid": [{"Id": 243, "Name": "Clarksdale, MS", "Image": "https://upload.wikimedia.org/wikipedia/commons/8/8c/Coahoma_County_Mississippi_Incorporated_and_Unincorporated_areas_Clarksdale_Highlighted.svg", "Model": "communities", "Info": {"Name": "Clarksdale, MS", "State": "Mississippi", "Counties": "Coahoma County", "Population": 17962}}, {"Id": 244, "Name": "Brooklyn, IL", "Image": "https://upload.wikimedia.org/wikipedia/commons/b/bf/Clair_County_Illinois_Incorporated_and_Unincorporated_areas_Brooklyn_Highlighted.svg", "Model": "communities", "Info": {"Name": "Brooklyn, IL", "State": "Illinois", "Counties": "St. Clair County", "Population": 749}}]}',
	'communities_sort1' : '{"num_pages": 135, "grid": [{"Id": 287, "Name": "Allendale, SC", "Image": "https://upload.wikimedia.org/wikipedia/commons/a/a8/SCMap-doton-Allendale.PNG", "Model": "communities", "Info": {"Name": "Allendale, SC", "State": "South Carolina", "Counties": "Allendale County", "Population": 3482}}]}',
	'communities_sort2' : '{"num_pages": 68, "grid": [{"Id": 266, "Name": "Booneville, KY", "Image": "https://upload.wikimedia.org/wikipedia/commons/9/9a/Downtown_Booneville.jpg", "Model": "communities", "Info": {"Name": "Booneville, KY", "State": "Kentucky", "Counties": "Owsley County", "Population": 81}}, {"Id": 318, "Name": "Moffat, CO", "Image": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Moffat%2C_Colorado.JPG/1024px-Moffat%2C_Colorado.JPG", "Model": "communities", "Info": {"Name": "Moffat, CO", "State": "Colorado", "Counties": "Saguache County", "Population": 116}}]}',
	'communities_filter1' : '{"num_pages": 21, "grid": [{"Id": 247, "Name": "Imperial, TX", "Image": "https://upload.wikimedia.org/wikipedia/commons/1/1a/Sand_dunes_at_Imperial%2C_TX_SCN1050.JPG", "Model": "communities", "Info": {"Name": "Imperial, TX", "State": "Texas", "Counties": "Pecos County", "Population": 278}}]}',
	'communities_search' : '{"num_pages": 20, "grid": [{"Id": 261, "Name": "Roma, TX", "Image": "https://upload.wikimedia.org/wikipedia/commons/1/12/TXMap-doton-Roma.PNG", "Model": "communities", "Info": {"Name": "Roma, TX", "State": "Texas", "Counties": "Starr County", "Population": 9765}}]}',
	'communities_charity_id' : '{"Communities": [{"Id": 247, "Name": "Imperial, TX"}, {"Id": 255, "Name": "Donna, TX"}, {"Id": 261, "Name": "Roma, TX"}, {"Id": 263, "Name": "Laredo, TX"}, {"Id": 264, "Name": "Edcouch, TX"}, {"Id": 267, "Name": "Fabens, TX"}, {"Id": 270, "Name": "Tivoli, TX"}, {"Id": 272, "Name": "La Joya, TX"}]}',
	'communities_school_district_id' : '{"Communities": [{"Id": 287, "Name": "Allendale, SC"}]}',
	'charities_id' : '{"Id": 353, "Name": "NCTA", "Image": "https://pbs.twimg.com/media/DXovG3QUQAA6gpC.png", "Info": {"Tagline": "A private, Christian, college-preparatory boarding school for highly-motivated students.  Over 90% on scholarship.", "Category": "Youth Education", "Scope": "Regional", "State": "Texas", "Website": "https://www.happyhillfarm.org", "Mission": "Founded in 1975, North Central Texas Academy (located on the beautiful, 500-acre campus of Happy Hill Farm) is an independent, accredited, coeducational, interdenominational college-preparatory, day, boarding, and international school.<br><br>North Central Texas Academy equips a unified, but diverse, student body for lifelong success by providing a comprehensive education from a Christian worldview that cultivates the mind, transforms the heart, and develops each student\'s character and God-given abilities.", "Rating": 3}}',
	'charities_page1' : '{"num_pages": 27, "grid": [{"Id": 182, "Name": "Jones Valley Teaching Farm", "Image": "https://jvtf.org/wp-content/uploads/2017/12/new-logo-stacked.png", "Model": "charities", "Info": {"Name": "Jones Valley Teaching Farm", "Tagline": "Encouraging students to act as critical thinkers, problem solvers, and change agents", "Category": "Youth Education", "Scope": "Regional", "State": "Alabama", "Rating": 2}}, {"Id": 183, "Name": "Junior Achievement of Alabama", "Image": "http://static1.squarespace.com/static/568d4405b204d5a4f88d2cbd/t/568d779d05f8e2ada343d666/1518216170222/?format=1500w", "Model": "charities", "Info": {"Name": "Junior Achievement of Alabama", "Tagline": "Empowering Young people to own their economic success", "Category": "Youth Education", "Scope": "Regional", "State": "Alabama", "Rating": 3}}, {"Id": 184, "Name": "Cornerstone Schools of Alabama", "Image": "https://csalabama.org/app/uploads/2017/09/cornerstone-school-vertical-logo.png", "Model": "charities", "Info": {"Name": "Cornerstone Schools of Alabama", "Tagline": "A model of excellence in urban education", "Category": "Youth Education", "Scope": "Regional", "State": "Alabama", "Rating": 3}}, {"Id": 185, "Name": "Better Basics", "Image": "https://pbs.twimg.com/profile_images/471317752284598273/0t6P-OnA.png", "Model": "charities", "Info": {"Name": "Better Basics", "Tagline": "Empowering Children - Reducing Illiteracy - Improving the Community", "Category": "Youth Education", "Scope": "Regional", "State": "Alabama", "Rating": 4}}, {"Id": 186, "Name": "Make Way for Books", "Image": "https://makewayforbooks.org/wp-content/themes/mwfb-theme/img/MWFB_Logowide_rgb.png", "Model": "charities", "Info": {"Name": "Make Way for Books", "Tagline": "Sharing stories changes lives, especially young lives", "Category": "Early Childhood", "Scope": "Regional", "State": "Arizona", "Rating": 4}}, {"Id": 187, "Name": "Phoenix Day", "Image": "https://static.wixstatic.com/media/528478_2f20825cd69b42fc9519eccf286bea38~mv2.png/v1/fill/w_387,h_120,al_c,q_80,usm_0.66_1.00_0.01/528478_2f20825cd69b42fc9519eccf286bea38~mv2.webp", "Model": "charities", "Info": {"Name": "Phoenix Day", "Tagline": "Building futures for children and families", "Category": "Early Childhood", "Scope": "Regional", "State": "Arizona", "Rating": 3}}, {"Id": 188, "Name": "Junior Achievement of Arizona", "Image": "https://media.licdn.com/dms/image/C4D0BAQEK-0VNlq3Z2w/company-logo_200_200/0?e=2159024400&v=beta&t=P8T4AFz-HYKR7_9Hjo1JUHxCY5sfEQinT9jAiZ0g7Y8", "Model": "charities", "Info": {"Name": "Junior Achievement of Arizona", "Tagline": "To inspire and prepare young people to succeed in a global economy Empowering young people to own their economic success", "Category": "Youth Education", "Scope": "Regional", "State": "Arizona", "Rating": 3}}, {"Id": 189, "Name": "Youth On Their Own", "Image": "https://yoto.org/wp-content/uploads/2015/06/yoto-header_01.png", "Model": "charities", "Info": {"Name": "Youth On Their Own", "Tagline": "Educating homeless teens, investing in Arizona\'s future!", "Category": "Youth Education", "Scope": "Regional", "State": "Arizona", "Rating": 3}}, {"Id": 190, "Name": "Families Forward Learning Center", "Image": "https://assets.networkforgood.org/21131/Images/Page/37cb562b-e20d-4895-8b90-cc9fe75f173c.jpg", "Model": "charities", "Info": {"Name": "Families Forward Learning Center", "Tagline": "A Two-Generation Learning Program", "Category": "Early Childhood", "Scope": "Regional", "State": "California", "Rating": 3}}]}',
	'charities_page2' : '{"num_pages": 121, "grid": [{"Id": 182, "Name": "Jones Valley Teaching Farm", "Image": "https://jvtf.org/wp-content/uploads/2017/12/new-logo-stacked.png", "Model": "charities", "Info": {"Name": "Jones Valley Teaching Farm", "Tagline": "Encouraging students to act as critical thinkers, problem solvers, and change agents", "Category": "Youth Education", "Scope": "Regional", "State": "Alabama", "Rating": 2}}, {"Id": 183, "Name": "Junior Achievement of Alabama", "Image": "http://static1.squarespace.com/static/568d4405b204d5a4f88d2cbd/t/568d779d05f8e2ada343d666/1518216170222/?format=1500w", "Model": "charities", "Info": {"Name": "Junior Achievement of Alabama", "Tagline": "Empowering Young people to own their economic success", "Category": "Youth Education", "Scope": "Regional", "State": "Alabama", "Rating": 3}}]}',
	'charities_sort1' : '{"num_pages": 242, "grid": [{"Id": 395, "Name": "A Better Chance", "Image": "https://pbs.twimg.com/profile_images/938745885978116098/jlMDgD23_400x400.jpg", "Model": "charities", "Info": {"Name": "A Better Chance", "Tagline": "Opening the door to greater educational opportunities since 1963", "Category": "Youth Education", "Scope": "National", "State": "New York", "Rating": 2}}]}',
	'charities_sort2' : '{"num_pages": 121, "grid": [{"Id": 363, "Name": "Readers Are Leaders Foundation", "Image": "http://d3n8a8pro7vhmx.cloudfront.net/readersareleaders/sites/2/meta_images/original/readers_are_leaders_logo.svg?1408667190", "Model": "charities", "Info": {"Name": "Readers Are Leaders Foundation", "Tagline": "Supporting literacy through newspapers in education", "Category": "Youth Education", "Scope": "Regional", "State": "Texas", "Rating": 0}}, {"Id": 393, "Name": "LEADership, Education and Development", "Image": "https://news.psu.edu/sites/default/files/styles/threshold-992/public/LEAD%20banner%203.jpg?itok=AtEQh8qs", "Model": "charities", "Info": {"Name": "LEADership, Education and Development", "Tagline": "Leadership, Education and Development", "Category": "Youth Education", "Scope": "National", "State": "Georgia", "Rating": 0}}]}',
	'charities_filter1' : '{"num_pages": 16, "grid": [{"Id": 350, "Name": "Educational First Steps", "Image": "http://www.socialventurepartners.org/dallas/wp-content/uploads/sites/52/2018/06/educational-first-steps.jpg", "Model": "charities", "Info": {"Name": "Educational First Steps", "Tagline": "One Childhood, One Chance", "Category": "Early Childhood", "Scope": "Regional", "State": "Texas", "Rating": 4}}]}',
	'charities_search' : '{"num_pages": 16, "grid": [{"Id": 350, "Name": "Educational First Steps", "Image": "http://www.socialventurepartners.org/dallas/wp-content/uploads/sites/52/2018/06/educational-first-steps.jpg", "Model": "charities", "Info": {"Name": "Educational First Steps", "Tagline": "One Childhood, One Chance", "Category": "Early Childhood", "Scope": "Regional", "State": "Texas", "Rating": 4}}]}',
	'charities_community_id' : '{"Charities": [{"Id": 182, "Name": "Jones Valley Teaching Farm"}, {"Id": 183, "Name": "Junior Achievement of Alabama"}, {"Id": 184, "Name": "Cornerstone Schools of Alabama"}, {"Id": 185, "Name": "Better Basics"}, {"Id": 365, "Name": "Jumpstart"}, {"Id": 366, "Name": "Reach Out and Read"}, {"Id": 367, "Name": "Readworks"}, {"Id": 368, "Name": "Children\'s Literacy Initiative"}]}',
	'charities_school_district_id' : '{"Charities": [{"Id": 348, "Name": "Communities in Schools of the Charleston Area"}, {"Id": 365, "Name": "Jumpstart"}, {"Id": 366, "Name": "Reach Out and Read"}, {"Id": 367, "Name": "Readworks"}, {"Id": 368, "Name": "Children\'s Literacy Initiative"}, {"Id": 369, "Name": "Barbara Bush Foundation for Family Literacy"}, {"Id": 370, "Name": "Reading Is Fundamental"}, {"Id": 371, "Name": "Ferst Readers"}]}',
	'all_entities_search1' : '{"num_pages": 7, "grid": [{"Id": 464, "Name": "Donna Independent School District", "Image": "http://www.donnaisd.net/Portals/Donna/District/images/DISD%20logo%20black%20feather.jpg?ver=2018-08-21-153142-663", "Model": "school_districts", "Info": {"Name": "Donna Independent School District", "State": "Texas", "Community": "Donna, TX", "Poverty": "55.8%", "Population": 29573}}, {"Id": 470, "Name": "Roma Independent School District", "Image": "https://www.romaisd.com//cms/lib/TX02215271/Centricity/Domain/4/2014_06_23_Mon%20roma%20isd%20logo%20seal.jpg", "Model": "school_districts", "Info": {"Name": "Roma Independent School District", "State": "Texas", "Community": "Roma, TX", "Poverty": "55.1%", "Population": 10261}}, {"Id": 472, "Name": "Laredo Independent School District", "Image": "http://p11cdn4static.sharpschool.com/UserFiles/Servers/Server_328908/Image/School%20Board/Members%20&%20bios/LSD.png", "Model": "school_districts", "Info": {"Name": "Laredo Independent School District", "State": "Texas", "Community": "Laredo, TX", "Poverty": "53.8%", "Population": 40665}}, {"Id": 473, "Name": "Edcouch-Elsa Independent School District", "Image": "http://p4cdn4static.sharpschool.com/UserFiles/Servers/Server_1220678/Image/Logo/EEISD%20Logo-Final%20Black.png", "Model": "school_districts", "Info": {"Name": "Edcouch-Elsa Independent School District", "State": "Texas", "Community": "Edcouch, TX", "Poverty": "53.7%", "Population": 9317}}, {"Id": 477, "Name": "Fabens Independent School District", "Image": "https://bloximages.newyork1.vip.townnews.com/elpasoinc.com/content/tncms/assets/v3/editorial/8/43/843fccc4-3735-11e7-b079-f3649adbd500/5915ec3a08e07.image.png", "Model": "school_districts", "Info": {"Name": "Fabens Independent School District", "State": "Texas", "Community": "Fabens, TX", "Poverty": "52.9%", "Population": 4219}}, {"Id": 480, "Name": "Austwell-Tivoli Independent School District", "Image": "https://www.atisd.net/cms/lib2/TX02218622/Centricity/Template/GlobalAssets/images///District/redfish-logo120.jpg", "Model": "school_districts", "Info": {"Name": "Austwell-Tivoli Independent School District", "State": "Texas", "Community": "Tivoli, TX", "Poverty": "51.9%", "Population": 249}}, {"Id": 482, "Name": "La Joya Independent School District", "Image": "https://sites.google.com/site/lajoyagrantsdevelopment/_/rsrc/1442263816511/home/LJISD%20Logo.jpg?height=416&width=455", "Model": "school_districts", "Info": {"Name": "La Joya Independent School District", "State": "Texas", "Community": "La Joya, TX", "Poverty": "51.7%", "Population": 52768}}, {"Id": 499, "Name": "Mercedes Independent School District", "Image": "https://www.misdtx.net/uploads/7/5/6/7/75678717/published/mercedes-isd-logo-final-color.png?1501380051", "Model": "school_districts", "Info": {"Name": "Mercedes Independent School District", "State": "Texas", "Community": "Mercedes, TX", "Poverty": "49.2%", "Population": 11496}}, {"Id": 506, "Name": "Lasara Independent School District", "Image": "https://www.lasaraisd.net/pages/uploaded_images/etch_logo.png", "Model": "school_districts", "Info": {"Name": "Lasara Independent School District", "State": "Texas", "Community": "Lasara, TX", "Poverty": "48.6%", "Population": 601}}]}',
}

class ViewsTestCase(TestCase):
	def setUp(self):
		self.factory = RequestFactory()

	# School District testing

	# Single school_district by id
	def test_school_districts_id(self):
		request = self.factory.get(request_inputs['school_districts_id'])
		response = school_districts(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['school_districts_id'])

	# Normal page
	def test_school_districts_page1(self):
		request = self.factory.get(request_inputs['school_districts_page1'])
		response = school_districts(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['school_districts_page1'])

	# Incorrect page number
	def test_school_districts_page2(self):
		request = self.factory.get(request_inputs['school_districts_page2'])
		response = school_districts(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['school_districts_page2'])

	# Sort by name
	def test_school_districts_sort1(self):
		request = self.factory.get(request_inputs['school_districts_sort1'])
		response = school_districts(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['school_districts_sort1'])

	# Sort by state
	def test_school_districts_sort2(self):
		request = self.factory.get(request_inputs['school_districts_sort2'])
		response = school_districts(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['school_districts_sort2'])

	# Filter by state
	def test_school_districts_filter1(self):
		request = self.factory.get(request_inputs['school_districts_filter1'])
		response = school_districts(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['school_districts_filter1'])

	# Search by state
	def test_school_districts_search(self):
		request = self.factory.get(request_inputs['school_districts_search'])
		response = school_districts(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['school_districts_search'])

	# List of school districts associated with given charity
	def test_school_districts_charity_id(self):
		request = self.factory.get(request_inputs['school_districts_charity_id'])
		response = school_districts(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['school_districts_charity_id'])

	# List of school districts associated with given community
	def test_school_districts_community_id(self):
		request = self.factory.get(request_inputs['school_districts_community_id'])
		response = school_districts(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['school_districts_community_id'])

	# Community Testing

	# Single communities by id
	def test_communities_id(self):
		request = self.factory.get(request_inputs['communities_id'])
		response = communities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['communities_id'])

	# Normal page
	def test_communities_page1(self):
		request = self.factory.get(request_inputs['communities_page1'])
		response = communities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['communities_page1'])

	# Incorrect page number
	def test_communities_page2(self):
		request = self.factory.get(request_inputs['communities_page2'])
		response = communities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['communities_page2'])

	# Sort by community
	def test_communities_sort1(self):
		request = self.factory.get(request_inputs['communities_sort1'])
		response = communities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['communities_sort1'])

	# Sort by population
	def test_communities_sort2(self):
		request = self.factory.get(request_inputs['communities_sort2'])
		response = communities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['communities_sort2'])

	# Filter by state
	def test_communities_filter1(self):
		request = self.factory.get(request_inputs['communities_filter1'])
		response = communities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['communities_filter1'])

	# Search by state
	def test_communities_search(self):
		request = self.factory.get(request_inputs['communities_search'])
		response = communities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['communities_search'])

	# List of communities associated with given charity
	def test_communities_charity_id(self):
		request = self.factory.get(request_inputs['communities_charity_id'])
		response = communities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['communities_charity_id'])

	# List of school districts associated with given school district
	def test_communities_school_district_id(self):
		request = self.factory.get(request_inputs['communities_school_district_id'])
		response = communities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['communities_school_district_id'])

	# Charity Testing

	# Single charity by id
	def test_charities_id(self):
		request = self.factory.get(request_inputs['charities_id'])
		response = charities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['charities_id'])

	# Normal page
	def test_charities_page1(self):
		request = self.factory.get(request_inputs['charities_page1'])
		response = charities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['charities_page1'])

	# Incorrect page number
	def test_charities_page2(self):
		request = self.factory.get(request_inputs['charities_page2'])
		response = charities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['charities_page2'])

	# Sort by name
	def test_charities_sort1(self):
		request = self.factory.get(request_inputs['charities_sort1'])
		response = charities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['charities_sort1'])

	# Sort by rating
	def test_charities_sort2(self):
		request = self.factory.get(request_inputs['charities_sort2'])
		response = charities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['charities_sort2'])

	# Filter by state
	def test_charities_filter1(self):
		request = self.factory.get(request_inputs['charities_filter1'])
		response = charities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['charities_filter1'])

	# Search by state
	def test_charities_search(self):
		request = self.factory.get(request_inputs['charities_search'])
		response = charities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['charities_search'])

	# List of charities associated with given school district
	def test_charities_school_district_id(self):
		request = self.factory.get(request_inputs['charities_school_district_id'])
		response = charities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['charities_school_district_id'])

	# List of charities associated with given school district
	def test_charities_community_id(self):
		request = self.factory.get(request_inputs['charities_community_id'])
		response = charities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['charities_community_id'])

	# All testing

	# Searching all across all models
	def test_all_entities_search1(self):
		request = self.factory.get(request_inputs['all_entities_search1'])
		response = all_entities(request)
		self.assertEqual(response.content.decode("utf-8"), response_outputs['all_entities_search1'])

	# Handler testing

	def test_id_handler(self):
		request = self.factory.get(request_inputs['charities_id'])
		response = JsonResponse(id_handler(Charities, request))
		self.assertEqual(response.content.decode("utf-8"), response_outputs['charities_id'])

	def test_page_handler(self):
		request = self.factory.get(request_inputs['communities_page1'])
		response = JsonResponse(page_handler(Communities, request))
		self.assertEqual(response.content.decode("utf-8"), response_outputs['communities_page1'])

	def test_search_handler(self):
		request = self.factory.get(request_inputs['charities_search'])
		entity_list = Charities.objects.all()
		entity_list = search_handler(Charities, request, entity_list)
		paginator = Paginator(entity_list, 1)
		entity_page = paginator.page(1)
		return_list = [FORMAT_RETURN_ENTITY[Charities](entity) for entity in entity_page]
		response =  JsonResponse({"num_pages": paginator.num_pages, "grid": return_list})
		self.assertEqual(response.content.decode("utf-8"), response_outputs['charities_search'])

	def test_filter_handler(self):
		request = self.factory.get(request_inputs['school_districts_filter1'])
		entity_list = SchoolDistricts.objects.all()
		entity_list = filter_handler(SchoolDistricts, request, entity_list)
		paginator = Paginator(entity_list, 1)
		entity_page = paginator.page(1)
		return_list = [FORMAT_RETURN_ENTITY[SchoolDistricts](entity) for entity in entity_page]
		response =  JsonResponse({"num_pages": paginator.num_pages, "grid": return_list})
		self.assertEqual(response.content.decode("utf-8"), response_outputs['school_districts_filter1'])

	def test_sort_handler(self):
		request = self.factory.get(request_inputs['communities_sort2'])
		entity_list = Communities.objects.all()
		entity_list = sort_handler(Communities, request, entity_list)
		paginator = Paginator(entity_list, 2)
		entity_page = paginator.page(1)
		return_list = [FORMAT_RETURN_ENTITY[Communities](entity) for entity in entity_page]
		response =  JsonResponse({"num_pages": paginator.num_pages, "grid": return_list})
		self.assertEqual(response.content.decode("utf-8"), response_outputs['communities_sort2'])

	def test_get_related_entity_list(self):
		request = self.factory.get(request_inputs['school_districts_community_id'])
		response = JsonResponse({"School Districts": get_related_entity_list(SchoolDistricts, Communities, '353')})
		self.assertEqual(response.content.decode("utf-8"), response_outputs['school_districts_community_id'])
