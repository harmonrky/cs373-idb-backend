from django.http import *
from django.views.decorators.http import *
from django.core.paginator import *
from django.db.models import FieldDoesNotExist
from .models import SchoolDistricts, Communities, Charities
from itertools import chain
import requests
from itertools import chain
from .id_handler import id_handler
from .page_handler import page_handler, get_max_list_per_page, search_handler, FORMAT_RETURN_ENTITY, sort_handler, filter_handler
from .related_entity_handler import get_related_entity_list


def index(request):
	return HttpResponse("Hello world!")

MAX_LIST_ENTRIES = 8
SEARCH_URL = "https://idb-search-dot-cs373-idb-218121.appspot.com/search_doc"

@require_http_methods(["GET"])
def school_districts(request):

	try:
		if 'id' in request.GET:
			response = id_handler(SchoolDistricts, request)
		elif 'page' in request.GET:
			response = page_handler(SchoolDistricts, request)
		elif 'community_id' in request.GET:
			response = {"School Districts": get_related_entity_list(
				SchoolDistricts,
				Communities,
				request.GET['community_id'])
			}
		elif 'charity_id' in request.GET:
			response = {"School Districts": get_related_entity_list(
				SchoolDistricts,
				Charities,
				request.GET['charity_id'])
			}
		else:
			raise ValueError
	except:
		return JsonResponse(status=404, data={})
	else:
		return JsonResponse(response)

@require_http_methods(["GET"])
def communities(request):

	try:

		if 'id' in request.GET:
			response = id_handler(Communities, request)
		elif 'page' in request.GET:
			response = page_handler(Communities, request)
		elif 'school_district_id' in request.GET:
			school_district_id = request.GET['school_district_id']


			if school_district_id is None:
				raise ValueError
			SchoolDistrict = SchoolDistricts.objects.get(pk=int(school_district_id))
			Communities_list = Communities.objects.filter(community=SchoolDistrict.community)[:MAX_LIST_ENTRIES]

			return_list = []
			for Community in Communities_list:
				return_dict = {
					"Id" : Community.id,
					"Name" : Community.community,
					}
				return_list.append(return_dict)
			response= {"Communities" : return_list}

		elif 'charity_id' in request.GET:
			charity_id = request.GET['charity_id']


			if charity_id is None:
				raise ValueError
			charity = Charities.objects.get(pk=int(charity_id))
			if charity.scope == 'Regional':
				Communities_list = Communities.objects.filter(state=charity.state)[:MAX_LIST_ENTRIES]
			else:
				Communities_list = Communities.objects.all()[:MAX_LIST_ENTRIES]



			return_list = []
			for Community in Communities_list:
				return_dict = {
					"Id" : Community.id,
					"Name" : Community.community,
					}
				return_list.append(return_dict)
			response = {"Communities" : return_list}
		else:
			raise ValueError
	except:
		return JsonResponse(status=404, data={})
	else:
		return JsonResponse(response)

@require_http_methods(["GET"])
def charities(request):
	MAX_LIST_PER_PAGE = 9
	try:
		if 'id' in request.GET:
			response = id_handler(Charities, request)
		elif 'page' in request.GET:
			response = page_handler(Charities, request)

		elif 'school_district_id' in request.GET:
			school_district_id = request.GET['school_district_id']


			if school_district_id is None:
				raise ValueError
			SchoolDistrict = SchoolDistricts.objects.get(pk=int(school_district_id))
			Charities_list = Charities.objects.filter(state=SchoolDistrict.state, scope="Regional")[:MAX_LIST_ENTRIES]


			return_list = []
			for Charity in Charities_list:
				return_dict = {
					"Id" : Charity.id,
					"Name" : Charity.name,
					}
				return_list.append(return_dict)

			# If there aren't enough state charities, expand our search to national
			if Charities_list.count() < MAX_LIST_ENTRIES:

				national_list_size = MAX_LIST_ENTRIES - Charities_list.count()
				Charities_list = Charities.objects.filter(scope="National")[:national_list_size]

				for Charity in Charities_list:
					return_dict = {
						"Id" : Charity.id,
						"Name" : Charity.name,
						}
					return_list.append(return_dict)
			return JsonResponse({"Charities" : return_list})

		elif 'community_id' in request.GET:
			community_id = request.GET['community_id']


			if community_id is None:
				raise ValueError
			Community = Communities.objects.get(pk=int(community_id))
			Charities_list = Charities.objects.filter(state=Community.state, scope="Regional")[:MAX_LIST_ENTRIES]

			return_list = []
			for Charity in Charities_list:
				return_dict = {
					"Id" : Charity.id,
					"Name" : Charity.name,
					}
				return_list.append(return_dict)

			# If there aren't enough state charities, expand our search to national
			if Charities_list.count() < MAX_LIST_ENTRIES:

				national_list_size = MAX_LIST_ENTRIES - Charities_list.count()
				Charities_list = Charities.objects.filter(scope="National")[:national_list_size]

				for Charity in Charities_list:
					return_dict = {
						"Id" : Charity.id,
						"Name" : Charity.name,
						}
					return_list.append(return_dict)
			return JsonResponse({"Charities" : return_list})

	except:
		return JsonResponse(status=404, data={})
	else:
		return JsonResponse(response)


@require_http_methods(["GET"])
def all_entities(request):
	try:

		page_num = request.GET['page']
		if page_num is None:
			raise ValueError
		MAX_LIST_PER_PAGE = get_max_list_per_page(request)
		search_term = request.GET['search']
		if search_term is None or search_term == "":
			raise ValueError
		# Searching
		SchoolDistricts_list = search_handler(SchoolDistricts, request, SchoolDistricts.objects.all())
		Communities_list = search_handler(Communities, request, Communities.objects.all())
		Charities_list = search_handler(Charities, request, Charities.objects.all())

		all_list = list(chain(SchoolDistricts_list, Communities_list, Charities_list))

		paginator = Paginator(all_list, MAX_LIST_PER_PAGE)
		all_page = paginator.page(page_num)

		return_list = [FORMAT_RETURN_ENTITY[type(entity)](entity) for entity in all_page]

		response = {"num_pages": paginator.num_pages, "grid": return_list}
	except:
		return JsonResponse(status=404, data={})
	else:
		return JsonResponse(response)
