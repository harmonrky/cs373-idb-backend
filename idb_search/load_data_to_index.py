import json
import time

import requests

BASE_URL = "http://api.equaleducation.info/"
# BASE_URL_INDEX = "http://localhost:8080"
BASE_URL_INDEX = "https://idb-search-dot-cs373-idb-218121.appspot.com"
NUM_PAGES = "num_pages"
GRID = "grid"
ID = "Id"
INFO = "Info"
IMAGE = "Image"

SCHOOL_DISTRICS = "school_districts"
COMMUNITIES = "communities"
CHARITIES = "charities"

def fetch_from_url(url):
    response = requests.get(url)
    if (response.ok):
        jData = json.loads(response.content)
        return jData
    else:
        response.raise_for_status()


def fetch_grid_data(entity, page_num):
    url = BASE_URL + entity + "/?page="+str(page_num)
    return fetch_from_url(url)


def fetch_instance_data(entity, entity_id):
    url = BASE_URL + entity + "/?id=" + str(entity_id)
    return fetch_from_url(url)


def fetch_ids(entity):
    data = fetch_grid_data(entity,1)
    page_num = data[NUM_PAGES]
    ids = []
    for p in range(1, page_num+1):
        data = fetch_grid_data(entity, p)
        ids.extend((x[ID] for x in data[GRID]))
    return ids


def transform_data(instance_data):

    for k in instance_data[INFO]:
        instance_data[INFO+"_"+k] = instance_data[INFO][k]
    instance_data.pop(INFO)
    instance_data.pop(IMAGE)
    return instance_data

def fetch_all_data(entity):
    ids = fetch_ids(entity)
    print(len(ids))
    all_data = []

    try:
        for i, curr_id in enumerate(ids):
            print i, curr_id
            all_data.append(transform_data(fetch_instance_data(entity, curr_id)))
    except BaseException:
        print("error")
    finally:

        return all_data


def add_data_to_index(content):

    url = BASE_URL_INDEX +"/add_doc"
    response = requests.post(url, data=json.dumps(content))
    if (response.ok):
        print ("sent data successfully")
    else:
        print ("some error")
        response.raise_for_status()

def add_instace_to_index(entity, data):
    new_data = {
        "entity": entity,
        "content": data
    }
    add_data_to_index(new_data)

def add_all_instace_to_index(entity):
    all_data = fetch_all_data(entity)
    for i,data in enumerate(all_data):
        print (str(i)+"/"+str(len(all_data)))
        add_instace_to_index(entity,data)

