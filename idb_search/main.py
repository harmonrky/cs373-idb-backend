#!/usr/bin/python
#
# Copyright 2011 Google Inc. All Rights Reserved.

from cgi import parse_qs
from datetime import datetime
import os
import string
import urllib
from urlparse import urlparse

import webapp2
from webapp2_extras import jinja2
import logging
from google.appengine.api import search

import json


ENTITY = "entity"
CONTENT = "content"
class BaseHandler(webapp2.RequestHandler):
    """The other handlers inherit from this class.  Provides some helper methods
    for rendering a template."""

    @webapp2.cached_property
    def jinja2(self):
      return jinja2.get_jinja2(app=self.app)

    def render_template(self, filename, template_args):
      self.response.write(self.jinja2.render_template(filename, **template_args))


def CreateDocument(content, doc_id_name = "Id"):
    """Creates a search.Document from content written by the author."""
    # Let the search service supply the document id.
    return search.Document(doc_id=str(content[doc_id_name]),
        fields=[search.TextField(name=x, value=content[x]) for x in content])


def CreateObject(doc):
    return {field.name: field.value for field in doc.fields}


class AddDoc(BaseHandler):
    def post(self):
        data = json.loads(self.request.body)
        logging.info(data)
        search.Index(name=data[ENTITY]).put(CreateDocument(data[CONTENT]))

class SearchDoc(BaseHandler):
    def get(self):
        query = self.request.get('query')
        entity = self.request.get('entity')
        results = search.Index(name=entity).search(query=query)

        self.response.content_type = 'application/json'
        if len(results.results):
            obj = {
                'results': [CreateObject(doc) for doc in results]
            }
        else:
            obj = {
                'results': []
            }
        self.response.write(json.dumps(obj))

def delete_all_in_index(index):

    while True:
        document_ids = [
            document.doc_id
            for document
            in index.get_range(ids_only=True)]

        if not document_ids:
            break
        index.delete(document_ids)


class DeleteAllDocs(BaseHandler):
    def post(self):

        entity = json.loads(self.request.body)[ENTITY]
        logging.info("deleting all docs in "+entity)
        delete_all_in_index(search.Index(name=entity))


application = webapp2.WSGIApplication(
    [
        ('/add_doc', AddDoc),
        ('/search_doc', SearchDoc),
        ('/delete_all_docs', DeleteAllDocs)
    ],
    debug=True)
